using restService.DTO;
using restService.Models;
using AutoMapper;
using restService.Request;

namespace restService.Mapper;

public class CustomAutoMapper : Profile
{
    public CustomAutoMapper()
    {
        CreateMap<Order, OrderDto>()
            .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => new CustomerDto
            {
                Id = src.Customer.Id,
                CustomerName = src.Customer.CustomerName,
                Email = src.Customer.Email
            }))
            .ForMember(dest => dest.Products,
                opt => opt.MapFrom(src => src.OrderProducts.Select(op => op.Product).ToList()))
            .ForMember(dest => dest.OrderDate,
                opt => opt.MapFrom(src => src.OrderDate.ToString("yyyy-MM-ddTHH:mm:ss")));

        CreateMap<Customer, CustomerDto>();
        CreateMap<Product, ProductDto>();
    }
}