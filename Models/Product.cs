namespace restService.Models;

public class Product
{
    public int Id { get; set; }
    public string ProductName { get; set; }
    public int Price { get; set; }
    public List<OrderProduct> OrderProducts { get; set; }
}