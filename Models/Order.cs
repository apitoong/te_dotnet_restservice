namespace restService.Models;

public class Order
{
    public int Id { get; set; }
    public DateTime OrderDate { get; set; }
    public int CustomerId { get; set; }
    public int Status { get; set; }
    public Customer Customer { get; set; }
    public List<OrderProduct> OrderProducts { get; set; }
}