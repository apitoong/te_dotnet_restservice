using System.ComponentModel.DataAnnotations;

namespace restService.Request;

public class OrderStatusUpdateRequest
{
    [Required(ErrorMessage = "Status is required")]
    public int Status { get; set; }
}