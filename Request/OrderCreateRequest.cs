using System.ComponentModel.DataAnnotations;

namespace restService.Request;

public class OrderCreateRequest
{
    [Required(ErrorMessage = "OrderDate is required")]
    public DateTime OrderDate { get; set; }

    [Required(ErrorMessage = "CustomerId is required")]
    public int CustomerId { get; set; }

    [Required(ErrorMessage = "Products is required")]
    public List<ProductInfo> Products { get; set; }
}

public class ProductInfo
{
    public int ProductId { get; set; }
    public int Quantity { get; set; }
}