using Microsoft.EntityFrameworkCore;
using restService.DTO;
using restService.Interface;
using restService.Models;
using restService.Request;

namespace restService.Usecase;

public class Order_usecase : IOrder_usecase
{
    protected string _source;
    protected readonly BasicLogger _basicLogger;
    private readonly BasicConfiguration _basicConfiguration;
    private readonly IOrder_repository _orderRepository;
    private readonly BasicDatabasse _dbContext;

    public Order_usecase(BasicLogger basicLogger, BasicConfiguration basicConfiguration,
        IOrder_repository orderRepository, BasicDatabasse dbContext)
    {
        _source = GetType().Name;
        _basicLogger = basicLogger;
        _basicConfiguration = basicConfiguration;
        _orderRepository = orderRepository;
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public async Task<List<OrderDto>> GetAllDataOrder()
    {
        try
        {
            var orderData = await _orderRepository.GetAllOrderAsync();
            return orderData;
        }
        catch (Exception e)
        {
            _basicLogger.Log("Error", "Debug", _source, e.Message);
            throw;
        }
    }

    public async Task<OrderDto> GetAllDataOrderById(int id)
    {
        try
        {
            var orderData = await _orderRepository.GetOrderDataByIdAsync(id);
            return orderData;
        }
        catch (Exception e)
        {
            _basicLogger.Log("Error", "Debug", _source, e.Message);
            throw;
        }
    }

    public async Task<int> CreateOrderUsecase(OrderCreateRequest request)
    {
        using var transaction = await _orderRepository.BeginTransaction();

        try
        {
            var payloadOrder = new Order
            {
                OrderDate = request.OrderDate,
                CustomerId = request.CustomerId
            };

            var orderId = await _orderRepository.CreateOrderAsync(payloadOrder);
            var orderProducts = request.Products.Select(productRequest => new OrderProduct
            {
                ProductId = productRequest.ProductId,
                Quantity = productRequest.Quantity
            }).ToList();

            await _orderRepository.CreateOrderProductsAsync(orderId, orderProducts);

            _orderRepository.CommitTransaction();
            return orderId;
        }
        catch (Exception ex)
        {
            _orderRepository.RollbackTransaction();
            _basicLogger.Log("Error", "Debug", _source, ex.Message);
            throw;
        }
    }

    public async Task UpdateOrderStatusAsync(int orderId, int status)
    {
        await _orderRepository.UpdateOrderStatusAsync(orderId, status);
    }

    public async Task DeleteOrderAsync(int orderId)
    {
        await _orderRepository.DeleteOrderAsync(orderId);
    }
}