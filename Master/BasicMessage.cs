namespace restService;

public class BasicMessage
{
    public static readonly string GeneralMessage = "Data Already Send";
    public static readonly string DataUpdated = "Data Already Updated";
    public static readonly string DataDeleted = "Data Already Deleted";
    public static readonly string GeneralErrorMessage = "Internal Server Error";
}