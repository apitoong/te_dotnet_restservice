namespace restService.Helpers;

public class Helper
{
    public static bool StringToBoolean(string value)
    {
        if (bool.TryParse(value, out var result))
            return result;

        return false;
    }

    public static int StringToInteger(string value)
    {
        if (int.TryParse(value, out var result))
            return result;

        return 0;
    }
}