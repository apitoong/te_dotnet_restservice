# mini backend  _(dotnet)_

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

#### Testing Machine:

    - MacOs 12.6.7
    - Docker version 24.0.2, build cb74dfc
    - dotnet version 6.0.416
    - mssqlserver 2022

## Installation

#### Manual install project Dotnet:

    1. clone repository   
    2. masuk ke folder hasil clone
    3. install dependensi jalankan "dotnet restore" 
    4. setup environtment pada file .env / appsettings.json 
        jika ingin menggunkan .env buka appsettings.json rubah "ISENV" menjadi true
        sebaliknya jika ingin menggunakan appsettings.json 
    5. jalankan migrasi "dotnet ef database update"
        jika ingin menggunakan query manual,
        file query ada di folder query     
    6. jalankan program "dotnet watch"
    7. arahkan ke {{baseurl}}/swagger/index.html

#### Note

- jika ada kendala di invite project github / postman mohon hubungi afitra - 085230010042

### http://apitoong.com
