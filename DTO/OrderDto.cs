using restService.Models;

namespace restService.DTO;

public class OrderDto
{
    public int Id { get; set; }
    public DateTime OrderDate { get; set; }
    public bool Status { get; set; }
    public CustomerDto Customer { get; set; }
    public List<ProductDto> Products { get; set; }
}