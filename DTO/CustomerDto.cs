namespace restService.DTO;

public class CustomerDto
{
    public int Id { get; set; }
    public string CustomerName { get; set; }
    public string Email { get; set; }
}