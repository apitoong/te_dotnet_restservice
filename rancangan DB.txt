Relasi 1-1 antara "Customers" dan "Orders":

Setiap pelanggan ("Customers") dapat memiliki nol atau satu pesanan ("Orders").
Kolom "id" di tabel "Customers" adalah kunci utama (PK).
Kolom "id" di tabel "Orders" adalah kunci utama (PK).
Kolom "CustomerID" di tabel "Orders" adalah kunci asing (FK) yang merujuk ke kolom "id" di tabel "Customers".
Relasi 1-n antara "Customers" dan "Orders":

Setiap pelanggan ("Customers") dapat memiliki satu atau banyak pesanan ("Orders").
Kolom "id" di tabel "Customers" adalah kunci utama (PK).
Kolom "id" di tabel "Orders" adalah kunci utama (PK).
Kolom "CustomerID" di tabel "Orders" adalah kunci asing (FK) yang merujuk ke kolom "id" di tabel "Customers".
Relasi n-n antara "Orders" dan "Products":

Setiap pesanan ("Orders") dapat memiliki banyak produk ("Products").
Setiap produk ("Products") dapat terkait dengan banyak pesanan ("Orders").
Kolom "id" di tabel "Orders" adalah kunci utama (PK).
Kolom "id" di tabel "Products" adalah kunci utama (PK).
Kolom "OrderID" di tabel "OrderProducts" adalah kunci asing (FK) yang merujuk ke kolom "id" di tabel "Orders".
Kolom "ProductID" di tabel "OrderProducts" adalah kunci asing (FK) yang merujuk ke kolom "id" di tabel "Products".