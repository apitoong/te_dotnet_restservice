-- Tabel untuk menyimpan informasi tentang pelanggan
CREATE TABLE Customers
(
    id           INT PRIMARY KEY,
    CustomerName VARCHAR(255) NOT NULL,
    Email        VARCHAR(255) NOT NULL,
    -- Other customer-related fields
);

-- Tabel untuk menyimpan informasi tentang pesanan
CREATE TABLE Orders
(
    id         INT PRIMARY KEY,
    OrderDate  DATE NOT NULL,
    CustomerID INT,
    Status     INT,
    -- Other order-related fields
    FOREIGN KEY (CustomerID) REFERENCES Customers (id)
);

-- Tabel untuk menyimpan informasi tentang produk
CREATE TABLE Products
(
    id          INT PRIMARY KEY,
    ProductName VARCHAR(255) NOT NULL,
    Price       INT          NOT NULL,
    -- Other product-related fields
);

-- Tabel penghubung untuk relasi n-n antara Orders dan Products
CREATE TABLE OrderProducts
(
    id        INT PRIMARY KEY,
    OrderID   INT,
    ProductID INT,
    Quantity  INT NOT NULL,
    FOREIGN KEY (OrderID) REFERENCES Orders (id),
    FOREIGN KEY (ProductID) REFERENCES Products (id)
);
