-- Seed data for Customers
INSERT INTO Customers (Id, CustomerName, Email)
VALUES (1, 'John Doe', 'john@example.com'),
       (2, 'Jane Doe', 'jane@example.com'),
       (3, 'Alice Smith', 'alice@example.com'),
       (4, 'Bob Johnson', 'bob@example.com'),
       (5, 'Eva Brown', 'eva@example.com'),
       (6, 'Mike Davis', 'mike@example.com'),
       (7, 'Laura Wilson', 'laura@example.com'),
       (8, 'Chris Lee', 'chris@example.com'),
       (9, 'Megan White', 'megan@example.com'),
       (10, 'Daniel Jones', 'a@mail.com');

-- Seed data for Orders
INSERT INTO Orders (Id, OrderDate, CustomerId, Status)
VALUES (1, '2023-12-01', 1, 0),
       (2, '2023-12-02', 2, 0),
       (3, '2023-12-03', 3, 0),
       (4, '2023-12-04', 4, 0),
       (5, '2023-12-05', 5, 0),
       (6, '2023-12-06', 6, 0),
       (7, '2023-12-07', 7, 0),
       (8, '2023-12-08', 8, 0),
       (9, '2023-12-09', 9, 0),
       (10, '2023-12-10', 10, 0);

-- Seed data for Products
INSERT INTO Products (Id, ProductName, Price)
VALUES (1, 'Product A', 1099),
       (2, 'Product B', 1999),
       (3, 'Product C', 599),
       (4, 'Product D', 799),
       (5, 'Product E', 1499),
       (6, 'Product F', 2499),
       (7, 'Product G', 399),
       (8, 'Product H', 899),
       (9, 'Product I', 1199),
       (10, 'Product J', 299);

-- Seed data for OrderProducts
INSERT INTO OrderProducts (Id, OrderId, ProductId, Quantity)
VALUES (1, 1, 1, 2),
       (2, 2, 2, 1),
       (3, 3, 3, 3),
       (4, 4, 4, 2),
       (5, 5, 5, 1),
       (6, 6, 6, 4),
       (7, 7, 7, 2),
       (8, 8, 8, 3),
       (9, 9, 9, 1),
       (10, 10, 10, 2);
