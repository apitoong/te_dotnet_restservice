using Microsoft.EntityFrameworkCore.Storage;
using restService.DTO;
using restService.Models;
using restService.Request;

namespace restService.Interface;

public interface IOrder_repository
{
    Task<List<OrderDto>> GetAllOrderAsync();
    Task<OrderDto> GetOrderDataByIdAsync(int id);
    Task<int> CreateOrderAsync(Order request);
    Task CreateOrderProductsAsync(int orderId, List<OrderProduct> orderProducts);
    Task UpdateOrderStatusAsync(int orderId, int status);
    Task DeleteOrderAsync(int orderId);
    Task<IDbContextTransaction> BeginTransaction();
    void CommitTransaction();
    void RollbackTransaction();
}