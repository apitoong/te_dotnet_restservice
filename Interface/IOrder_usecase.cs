using restService.DTO;
using restService.Models;
using restService.Request;

namespace restService.Interface;

public interface IOrder_usecase
{
    Task<List<OrderDto>> GetAllDataOrder();
    Task<OrderDto> GetAllDataOrderById(int id);
    Task<int> CreateOrderUsecase(OrderCreateRequest request);
    Task UpdateOrderStatusAsync(int orderId, int status);
    Task DeleteOrderAsync(int orderId);
}