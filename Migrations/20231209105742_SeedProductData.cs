﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace restService.Migrations
{
    public partial class SeedProductData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "ProductName", "Price" },
                values: new object[,]
                {
                    { 1, "Product A", 1099 },
                    { 2, "Product B", 1999 },
                    { 3, "Product C", 599 },
                    { 4, "Product D", 799 },
                    { 5, "Product E", 1499 },
                    { 6, "Product F", 2499 },
                    { 7, "Product G", 399 },
                    { 8, "Product H", 899 },
                    { 9, "Product I", 1199 },
                    { 10, "Product J", 299 },
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Products WHERE Id IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)");
        }
    }
}
