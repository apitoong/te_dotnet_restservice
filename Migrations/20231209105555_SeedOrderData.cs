﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace restService.Migrations
{
    public partial class SeedOrderData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "OrderDate", "CustomerId" },
                values: new object[,]
                {
                    { 1, DateTime.Parse("2023-12-01"), 1 },
                    { 2, DateTime.Parse("2023-12-02"), 2 },
                    { 3, DateTime.Parse("2023-12-03"), 3 },
                    { 4, DateTime.Parse("2023-12-04"), 4 },
                    { 5, DateTime.Parse("2023-12-05"), 5 },
                    { 6, DateTime.Parse("2023-12-06"), 6 },
                    { 7, DateTime.Parse("2023-12-07"), 7 },
                    { 8, DateTime.Parse("2023-12-08"), 8 },
                    { 9, DateTime.Parse("2023-12-09"), 9 },
                    { 10, DateTime.Parse("2023-12-10"), 10 },
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Orders WHERE Id IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)");
        }
    }
}
