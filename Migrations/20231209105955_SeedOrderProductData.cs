﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace restService.Migrations
{
    public partial class SeedOrderProductData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "OrderProducts",
                columns: new[] { "Id", "OrderId", "ProductId", "Quantity" },
                values: new object[,]
                {
                    { 1, 1, 1, 2 },
                    { 2, 2, 2, 1 },
                    { 3, 3, 3, 3 },
                    { 4, 4, 4, 2 },
                    { 5, 5, 5, 1 },
                    { 6, 6, 6, 4 },
                    { 7, 7, 7, 2 },
                    { 8, 8, 8, 3 },
                    { 9, 9, 9, 1 },
                    { 10, 10, 10, 2 },
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM OrderProducts WHERE Id IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)");
        }
    }
}
