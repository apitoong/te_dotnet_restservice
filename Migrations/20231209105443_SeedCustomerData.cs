﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace restService.Migrations
{
    public partial class SeedCustomerData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "CustomerName", "Email" },
                values: new object[,]
                {
                    { 1, "John Doe", "john@example.com" },
                    { 2, "Jane Doe", "jane@example.com" },
                    { 3, "Alice Smith", "alice@example.com" },
                    { 4, "Bob Johnson", "bob@example.com" },
                    { 5, "Eva Brown", "eva@example.com" },
                    { 6, "Mike Davis", "mike@example.com" },
                    { 7, "Laura Wilson", "laura@example.com" },
                    { 8, "Chris Lee", "chris@example.com" },
                    { 9, "Megan White", "megan@example.com" },
                    { 10, "Daniel Jones", "a@mail.com" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Customers WHERE Id IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)");
        }
    }
}
