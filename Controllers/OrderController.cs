using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using restService.Interface;
using restService.Request;
using restService.Usecase;

namespace restService.Controllers;

[Route("api/[controller]")]
[ApiController]
public class OrderController : BasicController
{
    private readonly IOrder_usecase orderUsecase;

    public OrderController(BasicLogger basicLogger, BasicConfiguration basicConfiguration, IMapper mapper,
        IOrder_usecase orderUsecase) : base(
        basicLogger, basicConfiguration, mapper)
    {
        _source = GetType().Name;
        this.orderUsecase = orderUsecase;
    }


    [HttpGet("all")]
    public async Task<IActionResult> GetOrderData()
    {
        try
        {
            var data = await orderUsecase.GetAllDataOrder();
            // return Ok(data);
            return SetResponse(StatusCodes.Status200OK, BasicCode.GeneralCode, true, BasicMessage.GeneralMessage, data);
        }
        catch (Exception e)
        {
            return SetErrorResponse(e);
        }
    }

    [HttpGet]
    [Route("{id:int}")]
    public async Task<IActionResult> GetOrderById([FromRoute] int id)
    {
        try
        {
            var data = await orderUsecase.GetAllDataOrderById(id);
            return SetResponse(StatusCodes.Status200OK, BasicCode.GeneralCode, true, BasicMessage.GeneralMessage, data);
        }
        catch (Exception e)
        {
            return SetErrorResponse(e);
        }
    }

    [HttpPost]
    public async Task<IActionResult> CreateOrder([FromBody] OrderCreateRequest request)
    {
        try
        {
            var data = await orderUsecase.CreateOrderUsecase(request);
            return SetResponse(StatusCodes.Status201Created, BasicCode.GeneralCode, true, BasicMessage.GeneralMessage,
                data);
        }
        catch (Exception e)
        {
            return SetErrorResponse(e);
        }
    }

    [HttpPut("status/{orderId}")]
    public async Task<IActionResult> UpdateOrderStatus(int orderId, [FromBody] OrderStatusUpdateRequest request)
    {
        try
        {
            await orderUsecase.UpdateOrderStatusAsync(orderId, request.Status);
            return SetResponse(StatusCodes.Status200OK, BasicCode.GeneralCode, true, BasicMessage.DataUpdated);
        }
        catch (Exception e)
        {
            return SetErrorResponse(e);
        }
    }

    [HttpDelete("{orderId}")]
    public async Task<IActionResult> DeleteOrder(int orderId)
    {
        try
        {
            await orderUsecase.DeleteOrderAsync(orderId);
            return SetResponse(StatusCodes.Status200OK, BasicCode.GeneralCode, true, BasicMessage.DataDeleted);
        }
        catch (Exception e)
        {
            return SetErrorResponse(e);
        }
    }
}