using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using restService.DTO;
using restService.Interface;
using restService.Models;
using restService.Request;

namespace restService.Repository;

public class Order_repository : IOrder_repository
{
    protected string _source;
    private readonly BasicDatabasse dbContext;
    private readonly IMapper mapper;
    protected readonly BasicLogger _basicLogger;

    public Order_repository(BasicLogger basicLogger, BasicDatabasse dbContext, IMapper mapper)
    {
        _source = GetType().Name;
        _basicLogger = basicLogger;
        this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        this.mapper = mapper;
    }


    public async Task<List<OrderDto>> GetAllOrderAsync()
    {
        var orders = await dbContext.Orders
            .Include(o => o.Customer)
            .Include(o => o.OrderProducts)
            .ThenInclude(op => op.Product)
            .ToListAsync();

        var orderDtos = orders.Select(order =>
        {
            var orderDto = mapper.Map<OrderDto>(order);
            orderDto.Customer = mapper.Map<CustomerDto>(order.Customer);
            orderDto.Products = order.OrderProducts.Select(op => mapper.Map<ProductDto>(op.Product)).ToList();

            return orderDto;
        }).ToList();

        return orderDtos;
    }

    public async Task<OrderDto> GetOrderDataByIdAsync(int id)
    {
        var order = await dbContext.Orders
            .Include(o => o.Customer)
            .Include(o => o.OrderProducts)
            .ThenInclude(op => op.Product)
            .FirstOrDefaultAsync(o => o.Id == id);
        if (order == null) return null;

        return mapper.Map<OrderDto>(order);
    }


    public async Task<int> CreateOrderAsync(Order order)
    {
        dbContext.Orders.Add(order);
        await dbContext.SaveChangesAsync();
        return order.Id;
    }

    public async Task CreateOrderProductsAsync(int orderId, List<OrderProduct> orderProducts)
    {
        try
        {
            foreach (var orderProduct in orderProducts)
            {
                orderProduct.OrderId = orderId;
                dbContext.OrderProducts.Add(orderProduct);
            }

            await dbContext.SaveChangesAsync();
        }
        catch (Exception ex)
        {
            _basicLogger.Log("Error", "Debug", _source, ex.Message);
            throw;
        }
    }

    public async Task UpdateOrderStatusAsync(int orderId, int status)
    {
        var order = await dbContext.Orders.FirstOrDefaultAsync(o => o.Id == orderId);

        if (order == null) throw new ArgumentException($"Order with ID {orderId} not found.");

        order.Status = status;

        await dbContext.SaveChangesAsync();
    }

    public async Task DeleteOrderAsync(int orderId)
    {
        var order = await dbContext.Orders.FirstOrDefaultAsync(o => o.Id == orderId);

        if (order == null) throw new ArgumentException($"Order with ID {orderId} not found.");

        dbContext.Orders.Remove(order);
        await dbContext.SaveChangesAsync();
    }

    public async Task<IDbContextTransaction> BeginTransaction()
    {
        return await dbContext.Database.BeginTransactionAsync();
    }

    public void CommitTransaction()
    {
        dbContext.Database.CommitTransaction();
    }

    public void RollbackTransaction()
    {
        dbContext.Database.RollbackTransaction();
    }
}